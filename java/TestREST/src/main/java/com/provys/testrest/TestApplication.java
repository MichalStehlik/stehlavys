/**
*/
package com.provys.testrest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author stehlik
 */
@ApplicationPath("TestApplication")
public class TestApplication extends Application {
}
